"""
9. Напишіть простий калькулятор, який зчитує три рядки, які вводить користувач: 
- перше число, друге число і операцію, 
- після чого застосовує операцію по введених числах («перше число» «операція» «друге число») 
і виводить результат на екран. 
Підтримувані операції: 
+, -, /, *, mod, pow, div, де mod - це взяття залишку від ділення, pow - піднесення до степеня, int_div - цілочисельне ділення. 
У випадку ділення на 0, необхідно використати обробник винятку, який має вивести повідомлення Division by zero!.
"""

"""
def sum(a, b):
    return a + b

def diff(a, b):
    return a - b

def div(a, b):
    return a / b

def mult(a, b):
    return a * b

def mod(a, b):
    return a % b

def pow(a, b):
    return a ** b

def int_div(a, b):
    return a // b


a = float(input("Enter first number: "))
b = float(input("Enter second number: "))
operation = str(input("Enter operation sign: "))

if operation == '+':
    print(f'a + b = {sum(a, b)}')
elif operation == '-':
    print(f'a - b = {diff(a, b)}')
elif operation == '/':
    try:
        result = div(a, b)
        print(f'a / b = {result}')
    except ZeroDivisionError:
        print('Division by zero!')
elif operation == '*':
    print(f'a * b = {mult(a, b)}')
elif operation == '%':
    print(f'a % b = {mod(a, b)}')
elif operation == '**':
    print(f'a ** b = {pow(a, b)}')
elif operation == '//':
    print(f'a // b = {int_div(a, b)}')
"""

"""
10. Виведіть імена видатних особистостей України, зображених на грошових знаках. 
Користувач вводить номінал банкноти . Програма виводить значення номіналу і ім’я особи, 
яка зображена на цій банкноті. 
Якщо користувач вводить неіснуюче значення номіналу, на екран виводиться відповідне повідомлення.
"""

"""
print()

cash = int(input("Enter denomination of the banknote: "))

cash_dict = {1: 'Володимир Великий', 2: 'Ярослав Мудрий', 5: 'Богдан Хмельницький', 10: 'Іван Мазепа'}

if cash in cash_dict:
    print(f'Denomination of the banknote: {cash}, depicted: {cash_dict[cash]}')
else:
    print('There are no such banknote.')
"""

"""
11. Створіть програму, яка зчитує позицію шахової фігури від користувача і повідомляє про колір квадрату, 
де знаходиться фігура. Наприклад, якщо користувач вводить a і 1, то програма повинна повідомити, 
що квадрат має колір black, якщо d і 5 - програма повідомляє, що квадрат має колір white.
"""

"""
print()

letter = str(input('Enter letter: '))
number = int(input('Enter number: '))

letter_list = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
number_list = list(range(1, 9))

if (letter in letter_list) and (number in number_list):
    if letter_list.index(letter) % 2 == number_list.index(number) % 2:
        print('Black')
    else:
        print('White')
else:
    print('Letter or number is out of the desk range.')
"""


"""
12. Чи є рядок «паліндромом»? 
Рядок є паліндромом, якщо він однаково читається зліва направо та справа наліво. 
Наприклад, слова Level, Noon, Anna є паліндромами, незалежно від регістру літер. 
Рядки, які треба перевірити, вводяться користувачем. 
Введення даних можна перервати, якщо ввести слово escape. 
Програма повинна вивести результат перевірки у вигляді повідомлення is a palindrome або is not a palindrome.
"""

"""
print()

while True:
    word = str(input("Enter word to check: "))
    word = word.lower()
    new_word = []
    
    if word == 'escape':
        break
    else:
        for i in word:
            new_word.insert(0, i)
        
        new_word = ''.join(new_word)
        
        if word == new_word:
            print('is a palindrome')
        else:
            print('is not a palindrome')
"""


"""
13. Створіть таблицю множення. Розмірність таблиці (кількість рядків і стовпців) вводиться з клавіатури.
"""

"""
print()

rows = int(input("Enter rows count: "))
columns = int(input("Enter columns count: "))

for i in range(1, rows + 1):
    for j in range (1, columns + 1):
        print(i * j, end='\t')
    print()
"""

"""
14. Як відомо, День програміста припадає на 256 день року, у невисокосний рік це 13 вересня, 
а у високосний - 12. 
Дізнайтеся число і номер місяця, що припадають на день, за номером n, який вводиться користувачем, 
у невисокосному 2017 році.
"""


print()

months_names_list = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
months_days_count_list = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

number = int(input('Enter number of day: '))
days_count = 0
month_count = 0
days_count_before_res_month = 0

while days_count < number:
    days_count += months_days_count_list[month_count]
    month_count += 1

for j in range(month_count - 1):
    days_count_before_res_month += months_days_count_list[j]

date = number - days_count_before_res_month


print(f"Month: {months_names_list[month_count - 1]}, date: {date}")