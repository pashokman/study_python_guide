"""
1. Виведіть вітальне повідомлення для кожного користувача після його входу на сайт. 
Cтворіть список з кількох імен користувачів, включаючи ім’я 'Admin'. 
Перебираючи елементи списку, виведіть повідомлення для кожного користувача. 
Для користувача з ім’ям 'Admin' виведіть особливе повідомлення - наприклад: 
- Hello Admin, I hope you’re well.. 
У інших випадках виводиться універсальне привітання - наприклад: 
- Hello Alex, thank you for logging in again.. 
Додайте команду if, яка перевірить, що список користувачів не порожній. 
Якщо список порожній, виведіть повідомлення: 
- We need to find some users!. 
Видаліть зі списку всі імена користувачів і переконайтеся у тому, що програма виводить правильне повідомлення.
"""


"""
users_list = ["Admin", "John", "Lisa", "Mary"]

if users_list == []:
    print("We need to find some users!")
else:
    for user in users_list:
        if user == "Admin":
            print(f"Hello {user}, I hope you’re well..")
        else:
            print(f"Hello {user}, thank you for logging in again..")
"""


"""
2. Визначте назву геометричної фігури за введеною кількістю її сторін. 
Програма повинна підтримувати фігури від 3 до 6 сторін. 
Якщо введена кількість сторін поза межами цього діапазону, програма повинна відображати відповідне повідомлення.
"""


"""
sides_count = int(input("Enter figure sides count: "))

if sides_count == 3:
    print("Triangle.")
elif sides_count == 4:
    print("Rectangle.")
elif sides_count == 5:
    print("Pentagon.")
elif sides_count == 6:
    print("Hexagon.")
else:
    print("Such a figure does not exist or it is not provided.")
"""


"""
3. Порядкові числівники у англійській мові закінчуються суфіксом th (окрім 1st, 2nd і 3rd). 
Cтворіть список чисел від 1 до 9. Використайте ланцюжок if-elif-else у циклі для виведення 
правильного закінчення числівника для кожного числа. 
Програма повинна виводити числівники 1st 2nd 3rd 4th 5th 6th 7th 8th 9th, кожен у новому рядку.
"""


"""
print()

end_list = list(range(1, 10))

for i in end_list:
    if i == 1:
        print(str(i) + "st")
    elif i == 2:
        print(str(i) + "nd")
    elif i == 3:
        print(str(i) + "rd")
    else:
        print(str(i) + "th")
"""


"""
4. Зчитайте ціле число введене користувачем і виведіть повідомлення про те, чи число парне або непарне.
"""


"""
print()

number = int(input("Enter integer number: "))

if number % 2 == 0:
    print("Number is even!")
else:
    print("Number is odd")
"""


"""
5. Зчитайте назву місяця від користувача як рядок і виведіть кількість днів у вказаному місяці.
Врахувати те, що «February» може мати 28 або 29 днів.
"""


"""
print()

month = str(input("Enter month name: "))
months_31_days = ["January", "March", "May", "July", "August", "October", "December"]
months_30_days = ["April", "June", "September", "November"]


if month in months_31_days:
    print(f"{month} have 31 days.")
elif month in months_30_days:
    print(f"{month} have 30 days.")
else:
    print(f"{month} have 28 or 29 days.")
"""


"""
6. Потрібно визначити, чи є даний рік високосним. 
Нагадаємо, що високосними роками вважаються ті роки, порядковий номер яких або кратний 4, 
але при цьому не кратний 100, або кратний 400 
(наприклад, 2000-й рік був високосним, а 2100-й буде невисокосним роком). 
Програма повинна коректно працювати на числах від 1900 до 3000 років. Виведіть Leap year. 
у разі, якщо рік є високосним і Ordinary year. у протилежному випадку.
"""

"""
print()


def is_leap_year(year):
    if (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0):
        print("Leap year.")
    else:
        print("Ordinary year.")


year = int(input('Enter year: '))
is_leap_year(year)
"""


"""
7. Зчитайте зі стандартного вводу цілі числа, по одному числу у рядку, 
і після першого введеного нуля виведіть суму отриманих на вхід чисел.
"""


"""
print()

input_list = []
value = None

while True:
    if value == 0:
        break
    else:
        value = int(input("Enter integer number: "))
        input_list.append(value)

sum = sum(input_list)
print(sum)
"""


"""
8. Зчитайте цілі числа, які вводить користувач, по одному числу у рядку. 
Для кожного введеного числа виконайте перевірку: 
- якщо число менше 10, то пропускаємо це число, 
- якщо число більше 100, то припиняємо зчитувати числа. 
У інших випадках вивести це число в окремому рядку.
"""


print()

input_list = []

while True:
    value = int(input("Enter integer number: "))

    if value < 10:
        input_list.append(" ")
    elif value > 100:
        break
    else:
        input_list.append(value)
        print(value)

print(input_list)
