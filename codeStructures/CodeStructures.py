"""
1. Напишіть код, який виводить різні повідомлення, в залежності від значення, що зберігається у
змінній weather_forecast: What a beautiful day!, якщо значення змінної дорівнює sun, Take an umbrella!, 
якщо значення змінної дорівнює rain і The sun’s just gone in! – у іншому випадку.
"""

"""
weather = str(input("Enter the weather: "))

if weather == "sun":
    weather_forecast = "What a beautiful day!"
elif weather == "rain":
    weather_forecast = "Take an umbrella!"
else:
    weather_forecast = "The sun’s just gone in!"

print(weather_forecast)
"""


"""
2. Створіть програму, яка виводить повідомлення про кількість очок, які отримав гравець у комп’ютерній грі. 
Створіть змінну з ім’ям points_color і надайте їй значення 'green', 'yellow' або 'red'. 
Якщо змінна points_color містить значення 'green', виведіть повідомлення про те, що гравець отримав 5 очок, 
якщо 'yellow' - 10 очок, якщо 'red', виведіть повідомлення про те, що гравець отримав 15 очок.
"""

"""
print()

points_color = "red"

if points_color == "green":
    print("Player get 5 points.")
elif points_color == "yellow":
    print("Player get 10 points.")
elif points_color == "red":
    print("Player get 15 points.")
"""

"""
3. Напишіть ланцюжок if-elif-else для визначення віку людини. 
Надайте значення змінній age, а потім виведіть повідомлення: 
- якщо значення age менше 2 - baby, 
- якщо значення age більше або дорівнює 2, але менше 4 - kid, 
- якщо значення age більше або дорівнює 4, але менше 13 - child, 
- якщо значення age більше або дорівнює 13, але менше 20 - teenager, 
- якщо значення age більше або дорівнює 20, але менше 65 - grown-up, 
- якщо значення age більше або дорівнює 65 - senior.
"""

"""
age = int(input("Enter your age: "))

if age < 2:
    print("baby")
elif age >= 2 and age < 4:
    print("kid")
elif age >= 4 and age < 13:
    print("child")
elif age >= 13 and age < 20:
    print("teenager")
elif age >= 20 and age < 65:
    print("grow-up")
else:
    print("senior")
"""


"""
4. Створіть список трьох своїх улюблених фруктів і назвіть його favorite_fruits. 
Напишіть перевірку на те, чи входить фрукт у список. 
Якщо фрукт входить у список, виводиться повідомлення, на зразок You really like peaches!, 
у протилежному випадку - повідомлення про відсутність фрукту у списку.
"""

"""
fruits = ['apple', 'banana', 'kiwi']
fruit = str(input("Enter fruit name: "))

if fruit in fruits:
    print(f"You really like {fruit}!")
else:
    print(f"{fruit.title()} is not in list.")
"""


"""
5. Використайте цикл for, щоб вивести на екран в окремих рядках значення списку [5, 4, 3, 2, 1, 0, 'GO!']
"""

"""
print()

list = [5, 4, 3, 2, 1, 0, "GO!"]

for item in list:
    print(item)
"""

"""
6. Використайте функцію zip(), щоб створити словник movies, який об’єднує у пари списки: 
seasons = ['summer', 'autumn']
months = [ 'july', 'november']. 
Виведіть вміст словника.
"""

"""
print()

seasons = ["summer", "autumn"]
months = ["july", "november"]


print(dict(zip(seasons, months)))
"""

"""
7. Використайте відомі вам структури коду для виведення ключів і значень словника 
activity = {'business': 'manager', 'it': 'software developer', 'science': 'scientist'} 
у вигляді, на зразок «категорія: професія»
"""

"""
print()

activity = {"business": "manager", "it": "software developer", "science": "scientist"}

for category, profession in activity.items():
    print(category, ":", profession)
"""

"""
8. Використайте включення списку, щоб створити список, який містить парні числа у діапазоні range(12).
"""

"""
print()

numbers = [number for number in range(12)]
print(numbers)
"""

"""
9. Використайте включення словника, щоб створити словник squares з ключами у вигляді цілих 
чисел з діапазону range(1, 11). 
Значення словника формуються піднесенням ключів до другого степеня.
"""

"""
print()

squares = {number: number**2 for number in range(1, 11)}
print(squares)
"""

"""
10. Використайте цикл for для виведення чисел від 1 до 15 включно, в один рядок і з пропусками між ними.
"""

"""
print()

for i in range(1, 16):
    print(i, end=" ")
"""

"""
11. Створіть список чисел від 1 до 1 000 000. Скористайтеся функціями min() і max() 
та переконайтеся у тому, що список дійсно починається 1 і закінчується 1 000 000. 
Викличте функцію sum() і подивіться, наскільки швидко Python зможе підсумувати мільйон чисел.
"""

"""
print(end="\n")

massive_list = [number for number in range(1, 1000001)]
# print(massive_list)
print(min(massive_list))
print(max(massive_list))
print(sum(massive_list))
"""

"""
12. Cкористайтеся третім аргументом функції range() для створення списку непарних чисел від 1 до 25 
і виведіть усі числа в окремих рядках у циклі for.
"""

"""
print()


odd_numbers = [number for number in range(1, 26, 2)]
print(odd_numbers)
"""

"""
13. Створіть список перших 10 кубів (тобто кубів усіх цілих чисел від 1 до 10) і виведіть значення усіх кубів
у циклі for в один рядок з пропусками.
"""

"""
print()

first_10_cub = [number**3 for number in range(1, 11)]

for item in first_10_cub:
    print(item, end=" ")
"""

"""
14. Cтворіть список чисел у діапазоні від 3 до 60 і виведіть усі числа списку у циклі while в окремих рядках.
"""

"""
print(end="\n")

list_3_60 = [number for number in range(3, 61, 1)]

i = 0
while i < len(list_3_60):
    print(list_3_60[i])
    i = i + 1
"""

"""
15. Визначте функцію trees, яка повертає список ['poplar', 'willow', 'lime']. Викличте функцію.
"""

"""
print()


def trees():
    return ["poplar", "willow", "lime"]


print(trees())
"""

"""
16. Напишіть функцію favorite_book(), яка отримує один параметр title. 
Функція повинна виводити повідомлення, на зразок One of my favorite books is "The Lord of the Rings".. 
Викличте функцію і переконайтеся у тому, що назва книги правильно передається як аргумент при виконанні функції.
"""

"""
print()


def favorite_book(title):
    print(f'One of my favorite books is "{title}".')


favorite_book("The Lord of the Rings")
"""

"""
17. Напишіть функцію make_shirt(), яка отримує розмір футболки і текст, який повинен бути надрукований на ній.
Функція повинна виводити повідомлення з розміром і текстом. 
Викличте функцію з використанням позиційних аргументів. 
Викличте функцію вдруге з використанням іменованих аргументів. 
Змініть функцію make_shirt(), щоб футболки за замовчуванням мали розмір XL, і на них виводився текст:
"I love Python!". 
Створіть футболку з розміром XL і текстом за замовчуванням.
"""

"""
print()


def make_shirt(size="XL", text="I love Python!"):
    print(f'Your shirt is {size} size and have text - "{text}".')


make_shirt("M", "Nice shirt!")
make_shirt(text="Bigger shirt!", size="XL")
make_shirt()
"""

"""
17. Напишіть функцію city_country(), яка отримує назву міста і країну. 
Функція повинна повертати рядок у форматі Kyiv, Ukraine. 
Викличте функцію як мінімум для трьох пар «місто-країна».
"""

"""
print()


def city_country(city_name, country_name):
    return f"{city_name.title()}, {country_name.title()}."


print(city_country("Kamianets-Podilskyi", "Ukraine"))
print(city_country("Poznan", "Poland"))
print(city_country("Rimini", "Italy"))
"""

"""
18. Визначте виняток, який називається MyException. 
Згенеруйте його, щоб побачити, що станеться. 
Потім напишіть код, що дозволяє перехопити цей виняток і вивести рядок This is my exception..
"""


print()

class MyException(Exception):
    "Raised when input value is not in the list"
    pass


sports_list = ['football', 'baseball', 'volleyball']
sports_dict = {name : name + ' ball' for name in sports_list}


try:
    value = str(input('Enter sport\'s name: '))
    if value not in sports_list:
        raise MyException
    else:
        print(sports_dict[value])
except MyException:
    print("This is my exception.")
else:
    print(sports_dict)
finally:
    print("Thank's for using this program!")


"""
19. При введенні числових даних часто зустрічається типова проблема: користувач вводить текст замість чисел. 
При спробі перетворити дані в int генерується виняток TypeError. 
Напишіть функцію, яка приймає два числа, шукає їх суму і виводить результат. 
Перехопіть виняток TypeError, якщо будь-яке із вхідних значень не є числом, 
і виведіть зручне повідомлення про помилку. 
Протестуйте функцію: спочатку введіть два числа, а потім введіть текст замість одного з чисел.
"""

"""
print()

try:
    num1 = int(input("Enter number1: "))
    num2 = int(input("Enter number2: "))

    sum = num1 + num2

    print(f"Sum of this two numbers is: {sum}")
except ValueError:
    print("ERROR! You need to input numbers!")
"""