"""
15. Конвертуйте десяткове число (з основою 10) у бінарне (двійкове, з основою 2). 
Користувач вводить десяткове число як ціле, а потім використовується алгоритм поділу (псевдокод), 
показаний нижче, щоб виконати перетворення. 
Коли алгоритм завершується, результат містить двійкове подання числа. 
Програма має вивести відповідне повідомлення, на зразок: 2018 in Decimal is 11111100010 in Binary. 
Модифікуйте програму таким чином, щоб вона виконувала зворотну операцію: 
перетворювала двійкове чило у десяткове.

Нехай result буде порожнім рядком
Нехай q представляє ціле число для перетворення
Повторити
	Встановити r рівним остачі, коли q ділиться на 2
	Перетворити r у рядок та додати його на початку до result
	Розділити q на 2, відкидаючи будь-який залишок, і зберегти результат назад у q
Поки q не дорівнює 0
Вивести повідомлення про результат переведення
"""

"""
print()

result = ''
q = int(input('Enter number to transformation: '))
start_number = q

while q != 0:
    r = q % 2
    r = str(r)
    result = r + result
    q = q // 2

print(f"{start_number} in Decimal is {result} in Binary")
"""

"""
Модифікуйте програму таким чином, щоб вона виконувала зворотну операцію: 
перетворювала двійкове чило у десяткове.
"""

"""
number = input("Enter number in Binary: ")
result = 0

for i in range(len(number)):
    result += int(number[i]) * 2 ** (len(str(number)) - i - 1)

print(f"{number} in Binary is {result} in Decimal")
"""

"""
16. Реалізуйте шифр Цезаря. Один з перших відомих прикладів шифрування був використаний Юлієм Цезарем. 
Цезар надавав письмові вказівки своїм генералам, але він не бажав, зрозуміло, щоб про них знали вороги. 
У результаті він створив власне шифрування, що згодом стало відоме як шифр Цезаря. 
Ідея цього шифру проста (і, як наслідок, вона не забезпечує серйозного захисту). 
Кожна буква в оригінальному повідомленні зміщується на 3 позиції. 
В результаті A стає D, B стає E, C стає F, D стає G і т. д. 
Останні три букви у алфавіті повертаються до початку: X стає A, Y стає B, а Z стає C. 
Нелітерні символи не враховуються шифром. 
Користувач вводить з клавітури повідомлення та зсув, а потім програма відображає зашифроване повідомлення. 
Переконайтеся, що програма шифрує як великі та малі літери. 
Програма також повинна підтримувати значення негативних зсувів, 
щоб її можна було використовувати як для кодування повідомлень, так і для декодування повідомлень. 
У нагоді стануть такі функції: 
ord() (перетворює символ у номер позиції цього символу у таблиці ASCII ) 
chr() (перетворює номер позиції символу у таблиці ASCII у відповідний символ).
"""

"""
print()

def enode_message(message, landslide):
    code = []
    coded_message = ''

    for i in message:
        code.append(ord(i) + landslide)

    for i in code:
        coded_message += chr(i)

    return coded_message


def decode_message(message, landslide):
    landslide = -landslide
    encode = []
    encoded_message = ''

    for i in message:
        encode.append(ord(i) + landslide)

    for i in encode:
        encoded_message += chr(i)

    return encoded_message


message = str(input('Enter your message: '))
landslide = int(input("Enter landslide: "))

hided_message = enode_message(message, landslide)
unhided_message = decode_message(hided_message, landslide)

print(f"Hided message: {hided_message}")
print(f"Unhided message: {unhided_message}")
"""


"""
17. У програмі визначте функцію, яка генерує випадковий пароль. 
Пароль має бути довільної довжини від 7 до 10 символів. 
Кожен символ паролю повинен бути випадково обраним з позицій 33 до 126 з таблиці кодів ASCII . 
Функція повертає випадково сформований пароль як єдиний її результат і він виводиться в основній програмі. 
Скористайтеся вказівками з попередньої задачі.
"""

"""
print()

import random
import string

length = random.randint(7, 10)
symbols = string.ascii_letters[33:126]

pwd = ''.join(random.choice(symbols) for i in range(length))

print("New generated password is:", pwd)
"""

"""
18. Напишіть програму, у якій комп’ютер генерує випадкове число, 
а користувач повинен вгадати число, вводячи його з клавіатури за вказану кількість спроб.
"""

"""
print()

import random

gen_number = random.randint(1, 10)

attempts = 3

while attempts > 0:
    attempts -= 1
    user_number = int(input("Gues the number from 1 to 10: "))
    
    if gen_number == user_number:
        print("You guess!")
    else:
        print("Try one more time!")
"""
        

"""
19. Створіть програму за мотивами відомої гри «Камінь, Ножиці, Папір». 
Застосуйте функціональний підхід при написанні коду програми.
"""


print()

import random

objects_list = ['Scissors', 'Rock', 'Paper']


def user1():
    return random.randint(0, 2)

def game():
    us1 = objects_list[user1()]
    us2 = objects_list[user1()]

    if (us1 == "Scissors" and us2 == 'Paper') or (us1 == "Paper" and us2 == 'Rock') or (us1 == "Rock" and us2 == 'Scissors'):
        print("1st Player won! Combination", us1, "vs", us2, ".")
    elif (us2 == "Scissors" and us1 == 'Paper') or (us2 == "Paper" and us1 == 'Rock') or (us2 == "Rock" and us1 == 'Scissors'):
        print("2st Player won! Combination", us2, "vs", us1, ".")
    else:
        print("Play one more time.")



game()