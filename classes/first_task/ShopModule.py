"""
6. Збережіть код класу Shop() у модулі. 
Створіть окремий файл, що імпортує клас Shop(). 
Створіть екземпляр all_store і викличте один з методів Shop(), 
щоб перевірити, що команда import працює правильно.
"""

class Shop():
    def __init__(self, shop_name, shop_type):
        self.shop_name = shop_name
        self.shop_type = shop_type
        self.number_of_units = 0

    def describe_shop(self):
        print(f'Our shop name is - "{self.shop_name}" and it\'s type - "{self.shop_type}".')
    
    def open_shop(self):
        print(f'Shop "{self.shop_name}" is open!')

    def set_number_of_units(self, number_of_units):
        self.number_of_units = number_of_units
        return self.number_of_units
    
    def increment_number_of_units(self, increment_number):
        self.number_of_units += increment_number
        return self.number_of_units