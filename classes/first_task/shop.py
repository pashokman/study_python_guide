"""
Задача 1. Онлайн-магазин.
1. Створіть клас з ім’ям Shop(). 
Метод __init__() класу Shop() повинен містити два атрибути: shop_name і store_type. 
Створіть метод describe_shop(), який виводить два атрибути, 
і метод open_shop(), який виводить повідомлення про те, що онлайн-магазин відкритий. 
Створіть на основі класу екземпляр з ім’ям store. 
Виведіть два атрибути окремо, потім викличте обидва методи.

2. Створіть три різних екземпляри класу, викличте для кожного екземпляру метод describe_shop().

3. Додайте атрибут number_of_units зі значенням за замовчуванням 0; 
він представляє кількість видів товару у магазині. 
Створіть екземпляр з ім’ям store. 
Виведіть значення number_of_units, а потім змініть number_of_units і виведіть знову.

4. Додайте метод з ім’ям set_number_of_units(), що дозволяє задати кількість видів товару. 
Викличте метод з новим числом, знову виведіть значення. 
Додайте метод з ім’ям increment_number_of_units(), який збільшує кількість видів товару на задану величину. 
Викличте цей метод.

5. Напишіть клас Discount(), що успадковує від класу Shop(). 
Додайте атрибут з ім’ям discount_products для зберігання списку товарів, на які встановлена знижка. 
Напишіть метод get_discounts_ptoducts, який виводить цей список. 
Створіть екземпляр store_discount і викличте цей метод.

6. Збережіть код класу Shop() у модулі. 
Створіть окремий файл, що імпортує клас Shop(). 
Створіть екземпляр all_store і викличте один з методів Shop(), 
щоб перевірити, що команда import працює правильно.
"""

class Shop():
    def __init__(self, shop_name, shop_type):
        self.shop_name = shop_name
        self.shop_type = shop_type
        self.number_of_units = 0

    def describe_shop(self):
        print(f'Our shop name is - "{self.shop_name}" and it\'s type - "{self.shop_type}".')
    
    def open_shop(self):
        print(f'Shop "{self.shop_name}" is open!')

    def set_number_of_units(self, number_of_units):
        self.number_of_units = number_of_units
        return self.number_of_units
    
    def increment_number_of_units(self, increment_number):
        self.number_of_units += increment_number
        return self.number_of_units


class Discount(Shop):
    def __init__(self):
        self.discount_products = ['cat', 'dog', 'snake']
    
    def get_discounts_products(self):
        print(self.discount_products)

print("1")
store1 = Shop("Candys", "All for 50 cen't")
print("Name:", store1.shop_name)
print("Type:", store1.shop_type)

store1.describe_shop()
store1.open_shop()

store2 = Shop("Equipment", "Military")
store3 = Shop("Cars", "Exotic")

print("2")
store1.describe_shop()
store2.describe_shop()
store3.describe_shop()

print("3")
store4 = Shop("Frieands", "Animals")
print("Number of units: ", store4.number_of_units)
store4.number_of_units = 10
print("Number of units: ", store4.number_of_units)

print("4")
store4.set_number_of_units(57)
print("Number of units: ", store4.number_of_units)

store4.increment_number_of_units(10)
print("Number of units: ", store4.number_of_units)

print("5")
store_discount = Discount()
store_discount.get_discounts_products()