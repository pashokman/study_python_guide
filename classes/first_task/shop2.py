"""
6. Збережіть код класу Shop() у модулі. 
Створіть окремий файл, що імпортує клас Shop(). 
Створіть екземпляр all_store і викличте один з методів Shop(), 
щоб перевірити, що команда import працює правильно.
"""

from ShopModule import Shop

all_store = Shop("Test Shop", "Test type")

all_store.increment_number_of_units(10)
print("Number of units", all_store.number_of_units)