"""
Задача 2. Облік користувачів на сайті.

1. Створіть клас з ім’ям User. 
Створіть два атрибути first_name і last_name, 
а потім ще кілька атрибутів, які зазвичай зберігаються у профілі користувача. 
Напишіть метод describe_user який виводить повне ім’я користувача. 
Створіть ще один метод greeting_user() для виведення персонального вітання для користувача. 
Створіть кілька примірників, які представляють різних користувачів. 
Викличте обидва методи для кожного користувача.

2. Додайте атрибут login_attempts у клас User. 
Напишіть метод increment_login_attempts(), що збільшує значення login_attempts на 1. 
Напишіть інший метод з ім’ям reset_login_attempts(), обнуляє значення login_attempts. 
Створіть екземпляр класу User і викличте increment_login_attempts() кілька разів. 
Виведіть значення login_attempts, щоб переконатися у тому, що значення було змінено правильно, 
а потім викличте reset_login_attempts(). 
Знову виведіть login_attempts і переконайтеся у тому, що значення обнулилося.

3. Адміністратор - користувач з повними адміністративними привілеями. 
Напишіть клас з ім’ям Admin, що успадковує від класу User. 
Додайте атрибут privileges для зберігання списку рядків виду:
«Allowed to add message», 
«Allowed to delete users», 
«Allowed to ban users» і т. д. 
Напишіть метод show_privileges() для виведення набору привілеїв адміністратора. 
Створіть екземпляр Admin і викличте метод.

4. Напишіть клас Privileges. 
Клас повинен містити всього один атрибут privileges зі списком, який треба забрати із класу Admin. 
Водночас, необхідно перемістити метод show_privileges() у клас Privileges із класу Admin. 
Створіть екземпляр priv як атрибут класу Admin. 
Створіть новий екземпляр admin і використайте метод для виведення списку привілеїв.

5. Збережіть клас User в одному модулі, а класи Privileges і Admin у іншому модулі. 
В окремому файлі створіть екземпляр admin і викличте метод show_privileges(), 
щоб перевірити, що все працює правильно.
"""

class User():
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name
        self.age = 0
        self.country = ''
        self.email = ''
        self.login_attempts = 0

    def describe_user(self):
        full_name = self.first_name + ' ' + self.last_name
        print(full_name)

    def greeting_user(self):
        print(f"Welcome, {self.first_name}!")

    def increment_login_attempts(self):
        self.login_attempts += 1
        return self.login_attempts

    def reset_login_attempts(self):
        self.login_attempts = 0
        return self.login_attempts        


class Admin(User):
    def __init__(self, first_name, last_name):
        super().__init__(first_name, last_name)
        self.priv = Privileges()

class Privileges():
    def __init__(self):
        self.privileges = ["Allowed to add message", "Allowed to delete users", "Allowed to ban users"]

    def show_privileges(self):
        print(self.privileges)


print("1")
user1 = User("Taras", "Shevchenko")
user2 = User("Lesia", "Ukrainka")

user1.describe_user()
user1.greeting_user()

user2.describe_user()
user2.greeting_user()

print("2")
user3 = User("Ivan", "Ohienko")

user3.increment_login_attempts()
user3.increment_login_attempts()
user3.increment_login_attempts()
user3.increment_login_attempts()
print(f"Loging attempts: {user3.login_attempts}")

user3.reset_login_attempts()
print(f"Loging attempts: {user3.login_attempts}")

print("3")
admin1 = Admin('Lina', 'Kostenko')
#admin1.show_privileges()       # not workin because privileges became a class from an attribute

print("4")
admin2 = Admin('Ivan', 'Franko')
admin2.priv.show_privileges()
