class User():
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name
        self.age = 0
        self.country = ''
        self.email = ''
        self.login_attempts = 0

    def describe_user(self):
        full_name = self.first_name + ' ' + self.last_name
        print(full_name)

    def greeting_user(self):
        print(f"Welcome, {self.first_name}!")

    def increment_login_attempts(self):
        self.login_attempts += 1
        return self.login_attempts

    def reset_login_attempts(self):
        self.login_attempts = 0
        return self.login_attempts        
