"""
5. Збережіть клас User в одному модулі, а класи Privileges і Admin у іншому модулі. 
В окремому файлі створіть екземпляр admin і викличте метод show_privileges(), 
щоб перевірити, що все працює правильно.
"""

from AdminModule import Admin

admin1 = Admin('Oleksandr', 'Dovshgenko')
admin1.priv.show_privileges()