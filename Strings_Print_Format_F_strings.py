import math

"""
1. Збережіть будь-яке повідомлення у змінній і виведіть це повідомлення. 
Потім замініть значення змінної іншим повідомленням і виведіть нове повідомлення. 
Програму збережіть у файлі, ім’я якої підпорядковується стандартним правилам Python по використанню малих 
літер і символів підкреслення - наприклад, simple_messages.py
"""


message = "For now Python is easy!"
print(message)

message = "But it is not true."
print(message)


"""
2. Збережіть ім’я користувача у змінній і виведіть повідомлення, призначене для конкретної людини. 
Повідомлення повинно бути, наприклад, таким: "Hello, Sasha, would you like to learn some Python today?"
"""

print()

user_name = 'Sasha'
print("Hello, {}, would you like to learn some Python today?".format(user_name))


"""
3. Знайдіть відому цитату, яка вам подобається. Збережіть ім’я автора вислову у змінній famous_person. 
Cкладіть повідомлення і збережіть його у новій змінній з ім’ям message. 
Виведіть своє повідомлення. Результат повинен виглядати приблизно так (включаючи лапки): 
Albert Einstein once said, "A person who never made a mistake never tried anything new.".
"""

print()

famous_person = "Vin Drosel"
message = "Race for the racers"

result = "{} once said, \"{}\".".format(famous_person, message)
print(result)


"""
4. Збережіть ім’я користувача у змінній і додайте на початку і у кінці імені кілька пропусків. 
Простежте за тим, щоб кожна керуюча послідовність (\t і \n) зустрічалася принаймні один раз. 
Виведіть ім’я, щоб було видно пропуски на початку і у кінці рядка. 
Потім виведіть його знову з використанням кожної з функцій видалення пропусків: lstrip(), rstrip() і strip()
"""
print()

user_name1 = "   \tAlberto\n  "
print(user_name1)
print(user_name1.lstrip())
print(user_name1.rstrip())
print(user_name1.strip())


"""
5. Використайте функцію print() для виведення повної домашньої адреси. 
У першому рядку виведіть власне ім’я та прізвище. 
У кожному наступному рядку виведіть окремі елементи адреси 
(країна, індекс, назва населеного пункту, вулиця, номер будинку тощо).
"""

print()
print("Pavlo Lekhitskyi\nUkraine\n32300\nKamianets-Podilskyi\nShevchenka street\nbuilding #10\nflat 65")


"""
6. Виконайте переведення одиниць вимірювання відстаней. Значення відстані вказано у метрах. 
У кожному новому рядку програма виводить значення відстані, представлене у:
дюймах, футах, милях, ярдах тощо. 
Числові дані на екрані мають бути у відформатованому вигляді: два знаки після десяткової крапки. 
Використайте функцію format(). Потрібні значення одиниць вимірювання знайдіть у мережі Інтернет.
"""
print()

meters = 150
inches = meters * 39.37
foots = meters * 3.28
miles = meters * 0.00062
yards = meters * 1.094

print("{} meters is equal to:\nInches - {}\nFoots - {}\nMiles - {}\nYards - {}".format( + \
    meters, inches, foots, miles, yards))

"""
7. Обчисліть тривалість якоїсь події. Припустимо, учнівські канікули тривали кілька днів. 
На екран треба вивести у відформатованому вигляді (вирівнювання за лівим краєм, ширина поля: 10 знаків)
загальну тривалість цієї події у годинах, хвилинах, секундах.
"""

print()

holiday = 5
hours = holiday * 24
minutes = hours * 60
seconds = minutes * 60

print("Holiday duration is: {0:<10d} hours or {1:<10d} minutes or {2:<10d} seconds.".format(hours, minutes, seconds))

"""
8. Виконайте перетворення значення температури у градусах Цельсія (C) для інших температурних шкал: 
Фаренгейта (F) і Кельвіна (K). 
Програма повинна відображати еквівалентну температуру у градусах Фаренгейта (F = 32 + 9/5 * C). 
Програма повинна відображати еквівалентну температуру у градусах Кельвіна (K = C + 273,15). 
Результати потрібно вивести на екран у відформатованому вигляді: 
з використанням двох знаків після десяткової крапки, мінімальною довжиною поля (15), 
вирівнюванням по центру. 
Зверніть увагу, у дійсних числах для розділення дробової і цілої частин використовують крапку.
"""

print()

c_temp = 25
f_temp = round(35 + 9 / 5 * c_temp, 2)
k_temp = round(c_temp + 273.15, 2)

print("Celsium temperature {0:^15} is equal to: {1:^15} F or {2:^15} K.".format(c_temp, f_temp, k_temp))


"""
9. Виконайте розкладання чотирицифрового цілого числа і виведіть на екран суму цифр у числі. 
Наприклад, якщо обрали число 6259, то програма повинна вивести на екран повідомлення: 6 + 2 + 5 + 9 = 22. 
Використайте функцію format() для відображення результату або f-рядки.
"""

print()

number = 6259
thousands = int(str(number)[0])
hundreds = int(str(number)[1])
dozens = int(str(number)[2])
oneces = int(str(number)[3])

sum = thousands + hundreds + dozens + oneces

string = f'{thousands} + {hundreds} + {dozens} + {oneces} = {thousands + hundreds + dozens + oneces}'
string1 = "{0} + {1} + {2} + {3} = {4}".format(thousands, hundreds, dozens, oneces, sum)
print("f-string: ", string)
print("string with format:", string1)


"""
10. За координатами широти і довготи двох точок на Землі у градусах визначте відстань між ними у кілометрах. 
Нехай (x1, y1) і (x2, y2) є кординатами широти і довготи (у градусах) двох точок на земній поверхні. 
Відстань між цими точками у кілометрах обчислюється так: 
6371.032 × arccos(sin(x1) × sin(x2) + cos(x1) × cos(x2) × cos(y1 - y2)). 
Значення 6371,032 - це середній радіус Землі у кілометрах. 
Тригонометричні функції Python працюють з радіанами. 
Як результат, необхідно перетворити значення координат із градусів у радіани перед обчисленням 
відстані за формулою. Модуль math містить функцію з ім’ям radians(), яка переводить градуси у радіани. 
Переведення можна зробити і за формулою, на зразок x1 = x1 × pi/180, де pi - число Пі. 
Знайдіть відстань між двома містами Пекін (39.9075000, 116.3972300) і Київ (50.4546600, 30.5238000) 
і виведіть значення на екран. Значення відстані повинне відображатися у відформатованому вигляді: 
з використанням трьох знаків після десяткової крапки, мінімальною довжиною поля (10), 
вирівнюванням за правим краєм.
"""

print()

coord_x1_degrees = 39.9075000
coord_y1_degrees = 116.3972300
coord_x2_degrees = 50.4546600
coord_y2_degrees = 30.5238000

coord_x1_radians = math.radians(coord_x1_degrees)
coord_y1_radians = math.radians(coord_y1_degrees)
coord_x2_radians = math.radians(coord_x2_degrees)
coord_y2_radians = math.radians(coord_y2_degrees)

distance = round(6371.032 * math.acos(math.sin(coord_x1_radians) * math.sin(coord_x2_radians) + \
math.cos(coord_x1_radians) * math.cos(coord_x2_radians) * math.cos(coord_y1_radians - coord_y2_radians)), 3)

print("Distance from Beijing to Kyiv: {:<10} km".format(distance))