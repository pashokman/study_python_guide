string = "I'd like to travel to Shanghai"[0:21]
print(string)

string = "I'd like to travel to Shanghai"[:21]
print(string)

string = "I'd like to travel to Shanghai"[22:]
print(string)

string = "I'd like to travel to Shanghai"[12:18]
print(string)

string = "I'd like to travel to Shanghai".upper()
print(string)

string = "I'd like to travel to Shanghai".upper().lower()
print(string)

string = "I'd like to travel to Shanghai".split()
print(string)

string = '>>'.join("I'd like to travel to Shanghai".split())
print(string)