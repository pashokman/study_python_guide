import math


def cylinder_volume(r, h):
    return math.pi * r**2 * h


def bigger_cylinder(volume1, volume2):
    if volume1 > volume2:
        print("Cylinder 1 is larger!")
    else:
        print("Cylinder 2 is larger!")
