"""
2. Візьміть за основу одну з написаних вами програм з однією/кількома функціями. 
Збережіть ці функції в окремому файлі. 
Імпортуйте функції у файл основної програми і викличте функцію будь-яким із таких способів:

імпорт ім'я_модуля
from ім'я_модуля import ім'я_функції
from ім'я_модуля import ім'я_функції as псевдонім
import ім'я_модуля as псевдонім
"""

import functions as func

cylinder1 = func.cylinder_volume(5, 10)
cylinder2 = func.cylinder_volume(3, 8)

func.bigger_cylinder(cylinder1, cylinder2)