'''
1. Файл models.py містить програмний код, поданий нижче, що імітує друкування 3D-моделей різних об’єктів. 
Перенесіть функції print_models() і show_completed_models() у окремий файл з ім’ям printing_functions.py. 
Виконайте імпорт цих функцій у файл models.py, змінивши файл так, 
щоб у ньому імпортовані функції можна було використовувати.

def print_models(unprinted_designs, completed_models):
    """
        Імітує друк 3D-моделей, доки список не стане порожнім.
        Кожна модель після друку переміщується у completed_models.
    """
    while unprinted_designs:
        current_design = unprinted_designs.pop()
        # Імітація друку моделі на 3D-принтері.
        print("Printing model: " + current_design)
        completed_models.append(current_design)

def show_completed_models(completed_models):
    """Виводить інформацію про усі надруковані моделі."""
    print("\nThe following models have been printed:")
    for completed_model in completed_models:
        print(completed_model)
unprinted_designs = ['iphone case', 'robot pendant', 'dodecahedron']
completed_models = []
print_models(unprinted_designs, completed_models)
show_completed_models(completed_models)
'''


import printing_functions

unprinted_designs = ['iphone case', 'robot pendant', 'dodecahedron']
completed_models = []
printing_functions.print_models(unprinted_designs, completed_models)
printing_functions.show_completed_models(completed_models)