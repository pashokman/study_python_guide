"""
1. Словники Python можуть використовуватися для моделювання «справжнього» словника (назвемо його глосарієм). 
Оберіть кілька термінів з програмування (або із іншої області), які ви знаєте на цей момент. 
Використайте ці слова як ключі глосарію, а їх визначення - як значення. 
Виведіть кожне слово і його визначення у спеціально відформатованому вигляді. 
Наприклад, ви можете вивести:
- слово, потім двокрапка і визначення; 
- слово в одному рядку, а його визначення - з відступом в наступному рядку. 
Використовуйте символ нового рядка (\n) для вставки порожніх рядків між парами «слово: визначення» 
і символ табуляції для встановлення відступів (\t) у вихідних даних.
"""


glossary = {'if': 'condition operator', 'for': 'cycle operator', 'function': 'part of a progran that may be used multiple times'}

for key in glossary:
    print(key, ':', glossary[key])

for key in glossary:
    print(key, '\n', '\t', glossary[key])


"""
2. Cтворіть словник з трьома річками і регіонами, територією яких вони протікають. 
Одна з можливих пар «ключ: значення» - 'Amazon': 'South America'. 
Додайте ще дві пари «річка: регіон» у словник. 
Виведіть повідомлення із назвами річки і регіону - наприклад, «The Amazon runs through South America.» 
для усіх елементів словника, враховуючи те, що у повідомлення у відповідні місця підставляються назви річок 
і територій.
"""


print()

rivers_and_regions = {'Amazon': 'South America', 'Dnipro': 'Center Ukraine', 'Wusla': 'West Poland'}

for key in rivers_and_regions:
    print(f'The {key} runs through {rivers_and_regions[key]}.')



"""
3. Створіть словник з чотирма назвами мов програмування (ключі) та іменами розробників цих мов (значення). 
Виведіть по черзі для усіх елементів словника повідомлення типу:
My favorite programming language is Python. It was created by Guido van Rossum..
Видаліть, на ваш вибір, одну пару «мова: розробник» із словника. 
Виведіть словник на екран.
"""


print()

languages_and_developers = {'Python': 'Mark Lutz', 'JavaScript': 'Peat Kolins', 'C++': 'Anthony Mitchal', 'C#': 'Ada Lovelase'}
print(languages_and_developers)


for key in languages_and_developers:
    print(f'My favorite programming language is {key}. It was created by {languages_and_developers[key]}.')

del languages_and_developers['C++']
print(languages_and_developers)


"""
4. Створіть англо-німецький словник, який називається e2g, і виведіть його на екран. 
Слова для словника: stork / storch, hawk / falke, woodpecker / specht і owl / eule. 
Виведіть німецький варіант слова owl. Додайте у словник, на ваш вибір, ще два слова та їхній переклад. 
Виведіть окремо: словник; ключі і значення словника у вигляді списків.
"""


print()

e2g = {'stork': 'storch', 'hawk': 'falke', 'woodpecker': 'specht', 'owl': 'eule'}
print(e2g)

print(e2g['owl'])

e2g['cat'] = 'katze'
e2g['dog'] = 'hund'

print(e2g)
print(list(e2g.keys()))
print(list(e2g.values()))


"""
5. Створіть кілька словників, імена яких - це клички домашніх тварин. 
У кожному словнику збережіть інформацію про вид домашнього улюбленця та ім’я власника. 
Збережіть словники в списку з ім’ям pets. 
Виведіть кілька повідомлень типу Alex is the owner of a pet - a dog..
"""


print()

rick = {'animal': 'dog', 'breed': 'dachshund', 'owner_name': 'Pavlo Lekhitskyi'}
milka = {'animal': 'cow', 'breed': 'domestic', 'owner_name': 'Peat Lory'}
bayron = {'animal': 'goat', 'breed': 'funny', 'owner_name': 'Petro Mayak'}

pets = [rick, milka, bayron]


for pet in pets:
    print(f"{pet['owner_name']} is the owner of a pet - a {pet['animal']}")


"""
6. Виведіть представлення букви, яку вводить користувач, у символах азбуки Морзе . 
Фрагмент словника має такий вигляд: 
morse = {'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.'}. 
Передбачте у програмі обробку малих і великих букв.
"""

"""
print()

morse = {'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.'}

letter = str(input("Enter letter to convert: "))

print(morse[letter.upper()])
"""

"""
7. Створіть багаторівневий словник subjects навчальних предметів. 
Використайте наступні рядки для ключів верхнього рівня: 'science', 'humanities' і 'public'. 
Зробіть так, щоб ключ 'science' був ім’ям іншого словника, який має ключі:
'physics', 'computer science' і 'biology'. 
Зробіть так, щоб ключ 'physics' посилався на список рядків зі значеннями 'nuclear physics', 'optics' і 'thermodynamics'. 
Решта ключів повинні посилатися на порожні словники. 
Виведіть на екран ключі subjects['science'] і значення subjects['science']['physics'].
"""


print()


subjects = {
    'science': {
        'physics': ['nuclear physics', 'optics', 'thermodynamics'], 
        'computer science': {}, 
        'biology': {}
        }, 
    'humanities': {},
    'public': {}
    }

print(list(subjects['science'].keys()))
print(list(subjects['science']['physics']))


"""
8. Створіть словник з ім’ям cities. Використайте назви трьох міст в якості ключів словника. 
Створіть словник з інформацією про кожне місто: 
включіть в нього країну, в якій розташоване місто, приблизну чисельність населення і один цікавий факт про місто. 
Ключі словника кожного міста повинні називатися country, population і fact. 
Виведіть назву кожного міста і всю збережену інформацію про нього.
"""

print()

cities = {
    'Kamianets-Podilskyi': {
        'country': 'Ukraine', 
        'population': 300000, 
        'fact': 'flower on the stone'
        }, 
    'Poznan': {
        'country': 'Poland', 
        'population': 700000, 
        'fact': 'manufacturing city'
    }, 
    'Rimini': {
        'country': 'Italy', 
        'population': 150000, 
        'fact': 'beach city'
    }}

for city, prop in cities.items():
    print(f"{city} located in {prop['country']}, have population - {prop['population']}. Fact about a city - {prop['fact']}.")



"""
9. Із словника teams необхідно вивести на екран статистику кількох команд Національної баскетбольної асоціації NBA. 
Створіть словник teams, дотримуючись наступних правил:
 - Назви команд  - це ключі словника. 
 - Значення у словнику - це список, на зразок: [Всього ігор, Перемог, Нічиїх, Поразок, Всього очок]. 
 - Значення списку - це цілі числа, які обираються довільно. 
При виведенні даних, прослідкуйте, щоб формат виведення був таким: NEW YORK KNICKS 22 7 6 9 45. 
Інформація про кожну команду має міститися в окремому рядку. 
Зверніть увагу на те, що дані у словнику є невпорядкованими.
"""


print()

teams = {
    'Los Angeles Lakers': [70, 53, 4, 13, 44],
    'New York Knicks': [40, 15, 5, 20, 0],
    'Detroit Pistons': [65, 28, 7, 30, 5]
}

for team, prop in teams.items():
    print(f"{team} {prop[0]} {prop[1]} {prop[2]} {prop[3]} {prop[4]}")


"""
10. Ви створюєте пригодницьку гру і використовуєте для зберігання предметів гравця словник, 
у якому ключі - це назви предметів, значення - кількість одиниць кожної із речей. 
Наприклад, словник може виглядати так: 
things = {'key': 3, 'mace': 1, 'gold coin': 24, 'lantern': 1, 'stone': 10}. 
Виведіть повідомлення про усі речі гравця у такому вигляді:
Equipment:
3 key
1 mace
24 stone
1 lantern
10 gold coin
Total number of things: 39
"""


print()

things = {'key': 3, 'mace': 1, 'gold coin': 24, 'lantern': 1, 'stone': 10}
total_number = 0

print("Equipment:")
for thing, prop in things.items():
    print(f"{prop} {thing}")

for value in things.values():
    total_number += value 

print(f"Total number of things: {total_number}")
