"""
1. Збережіть назви мов світу (Ukrainian, French, Bulgarian, Norwegian, Latvian або інші) у списку. 
Простежте за тим, щоб елементи у списку не зберігались в алфавітному порядку. 
Застосуйте функції sorted(), reverse(), sort() до списку. 
Виведіть список на екран до і після використання кожної із функцій.
"""


languages = ["Ukrainian", "French", "Bulgarian", "Norwegian", "Latvian"]
print(languages)

print(sorted(languages))

languages.reverse()
print(languages)

languages.sort()
print(languages)


"""
2. На вхід програми подається один рядок з цілими числами. Числа розділені пропусками. 
Необхідно вивести суму цих чисел. Наприклад, якщо був введений рядок чисел 2 -1 9 6, 
то результатом роботи програми буде їх сума 16.
"""
"""
string = str(input("Enter string with integer numbers separated by spaces: "))
list = string.split(' ')
sum = 0

for element in list:
    sum += int(element)

print(f"Sum of entered integers is: {sum}")
"""

"""
3. Дано список з такими елементами: cities = ['Budapest', 'Rome', 'Istanbul', 'Sydney', 'Kyiv', 'Hong Kong']. 
Сформуйте з елементів списку повідомлення, у якому перед останнім елементом буде вставлено слово and. 
Наприклад, у нашому випадку, повідомлення буде таким: Budapest, Rome, Istanbul, Sydney, Kyiv and Hong Kong. 
Програма має працювати з будь-якими списками, довжина яких є 6.
"""


print()

cities = ['Budapest', 'Rome', 'Istanbul', 'Sydney', 'Kyiv', 'Hong Kong']
print(cities)

first_cities = cities[:4]
print(first_cities)

last_cities = cities[-2:]
print(last_cities)

new_element = [' and '.join(last_cities)]
print(new_element)

new_cities = first_cities + new_element
print(new_cities)

print(', '.join(new_cities))


"""
4. Необхідно зчитати рядок з 5 цифр, розділених пропусками, і зберегти кожну цифру у список. 
Створіть копію списку із впорядкованими елементами у зворотному порядку. 
Виведіть число, яке утворюється об’єднанням елементів нового списку.
"""

"""
numbers_string = str(input("Enter numbers separated by space: "))
list = numbers_string.split(' ')
print(list)

list_copy = list.copy()
print(list_copy)

list_copy.reverse()
print(list_copy)

print("".join(list_copy))
"""

"""
5. Поміркуйте над тим, яку інформацію можна було б зберігати у списку. 
Наприклад, створіть список професій, видів спорту, членів родини, назви океанів тощо, 
а потім викличте кожну функцію для роботи зі списками, яка була згадана у цьому розділі, хоча б один раз.
"""


proffesions = ["baker", "racer", "developer", "tester", "racer"]
print(proffesions)

copy_proffesions = proffesions.copy()
print(copy_proffesions)

print(len(proffesions))

print(', '.join(proffesions))

print(proffesions.index("developer"))

print(proffesions.count("racer"))

numbers = list(range(1, 10, 2))
print(numbers)
print(max(numbers))
print(min(numbers))
print(sum(numbers))

print("developer" in proffesions)
print("developer" not in proffesions)
print("designer" not in proffesions)

print(copy_proffesions)
print(sorted(copy_proffesions))
print(copy_proffesions)

copy_proffesions.sort()
print(copy_proffesions)


"""
Виконайте візуалізацію структури коду програми. 
Використайте у візуалізації структури елементи кортежу keywords = ('for', 'if', 'else', 'in', ':'). 
У процесі виведення структури коду на екран, враховуйте відступи рядків від лівого краю, 
у розрахунку один відступ - 4 пропуски. Вигляд структури коду має бути таким:

for each token in the postfix expression :
    if the token is a number :
        print('Convert it to an integer and add it to the end of values')
    else :
        print('Append the result to the end of values')
"""


keywords = ('for', 'if', 'else', 'in', ':')
list_test = ['first', 'second', 3, 'fourth']

"""
keywords[0] i keywords[-2] list keywords[-1]
    keywords[1] type(i) == 'number' keywords[-1]
        print('Convert it to an integer and add it to the end of values')
    keywords[2] keywords[-1]
        print('Append the result to the end of values')
"""